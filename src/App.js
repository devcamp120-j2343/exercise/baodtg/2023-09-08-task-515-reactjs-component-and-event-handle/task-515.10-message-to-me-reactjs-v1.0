import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import Content from './components/content/content';
import Header from './components/header/Header';

function App() {
  return (
    <div className="container text-center">
      <div>
        <Header />
      </div>
      <div>
        <Content />
      </div>
    </div>
  );
}

export default App;
