import { Component } from "react";

class InputContent extends Component {
    onInputChangeHandler = (event) => {
        let value = event.target.value
        console.log(event.target.value)
        this.props.inputMessageChangeHandlerProps(value)
    }
    onButtonClickHandler= () => {
        console.log("Ấn nút gửi thông điệp!")
        this.props.outputMessageChangeHandlerProps();
    }
    render() {
        return (
            <>
                <div className='row'>
                    <div className='col-12 mt-2'>
                        <label>Message cho bạn 12 tháng tới:</label>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12 mt-2'>
                        <input className='form-control w-75 mx-auto' placeholder='Nhập lời nhắn cho bạn...' onChange={this.onInputChangeHandler} value={this.props.inputMessageProps} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12 mt-2'>
                        <button className='btn btn-success' onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                    </div>
                </div>
            </>
        )
    }
}
export default InputContent