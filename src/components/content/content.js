import { Component } from "react";
import InputContent from "./input/InputContent";
import OutputContent from "./output/OutputContent";

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputMessage: "",
            outputMessage: [],
            likeDisplay: false
        }
    }
    inputMessageChangeHandler = (value) => {
        this.setState({
            inputMessage: value
        })
    }
    outputMessageChaneHandler = () => {
        if (this.state.inputMessage) {
            this.setState({
                outputMessage: [...this.state.outputMessage, this.state.inputMessage],
                likeDisplay: true
            })
        }else{
            this.setState({
                outputMessage: this.state.inputMessage,
                likeDisplay: false
            })
        }

    }
    render() {
        return (
            <>
                <InputContent inputMessageProps={this.state.inputMessage} inputMessageChangeHandlerProps={this.inputMessageChangeHandler} outputMessageChangeHandlerProps={this.outputMessageChaneHandler} />
                <OutputContent outputMessageProps={this.state.outputMessage} likeDisplayProps={this.state.likeDisplay} />
            </>
        )
    }
}
export default Content