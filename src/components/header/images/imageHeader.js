import { Component } from "react";
import image from '../../../assets/images/software-developer-6521720_640.webp';

class ImageHeader extends Component {
    render() {
        return (
            <div className='row'>
                <div className='col-12 mt-2'>
                    <img src={image} alt='Chào mừng đến với Devcamp120' width={'600px'}></img>
                </div>
            </div>
        )
    }
}
export default ImageHeader

