import { Component } from "react";
import TextHeader from "./text/textHeader";
import ImageHeader from "./images/imageHeader";

class Header extends Component {
    render() {
        return (
            <>
                <TextHeader />
                <ImageHeader />


            </>
        )
    }
}
export default Header